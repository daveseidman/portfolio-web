import { Vector3, Color, Object3D } from 'three';
import React, { useRef, useState } from 'react';
import { Canvas, useFrame } from '@react-three/fiber';
import { MarchingCubes, MarchingCube, Environment, Sky, Bounds } from '@react-three/drei';
import { Physics, RigidBody, BallCollider, interactionGroups } from '@react-three/rapier';
import { Camera as _Camera } from '@mediapipe/camera_utils';
import { Hands as _Hands } from '@mediapipe/hands';
import { useSpring, animated } from '@react-spring/three';
import inobounce from 'inobounce'; // eslint-disable-line
import Styles from './App.scss'; // eslint-disable-line
import envFile from './assets/satara_night_2k.hdr';
import { handMeshes, _leftHand, _rightHand } from './utils';

const vec = new Vector3();

function Pointer({ color }) {
  const ref = useRef();
  useFrame(({ mouse, viewport }) => {
    const { width, height } = viewport.getCurrentViewport();
    vec.set(mouse.x * (width / 2), mouse.y * (height / 2), 0);
    ref.current.setNextKinematicTranslation(vec);
  });
  return (
    <RigidBody type="kinematicPosition" colliders={false} ref={ref}>
      {/* <MarchingCube strength={0.5} subtract={10} color={color} /> */}
      <mesh scale={0.1}>
        <sphereGeometry />
        <meshStandardMaterial color={color} />
      </mesh>
      <BallCollider args={[0.1]} type="dynamic" />
    </RigidBody>
  );
}

const MetaBall = ({ color, position, groups }) => {  // eslint-disable-line
  const api = useRef();
  useFrame((state, delta) => {
    api.current.applyImpulse(vec.copy(api.current.translation()).normalize().multiplyScalar(delta * -0.05));
  });
  return (
    <RigidBody
      ref={api}
      colliders={false}
      linearDamping={4}
      angularDamping={0.95}
      position={position}
      collisionGroups={groups}
    >
      <MarchingCube strength={0.35} subtract={6} color={color} />
      <mesh scale={0.1}>
        <sphereGeometry />
        <meshStandardMaterial color={color} />
      </mesh>
      <BallCollider args={[0.1]} type="dynamic" />
    </RigidBody>
  );
};

function MetaBall2({ color, position }) {
  const ref = useRef();
  useFrame(() => {
    ref.current.setNextKinematicTranslation(position);
  });
  return (
    <RigidBody type="kinematicPosition" colliders={false} ref={ref}>
      <mesh scale={0.05}>
        <sphereGeometry />
        <meshStandardMaterial color={color} />
      </mesh>
      <BallCollider args={[0.05]} type="dynamic" />
    </RigidBody>
  );
}

let now = new Date();
let lastFrameTime = new Date();
const timeThreshold = 40;

export default function App() {
  const Hands = _Hands || window.Hands;
  const Camera = _Camera || window.Camera;

  const [cameraStatus, setCameraStatus] = useState('stopped');
  const [camera, setCamera] = useState(null);

  const [leftHand, setLeftHand] = useState(_leftHand);
  const [rightHand, setRightHand] = useState(_rightHand);
  const _hands = { left: leftHand, right: rightHand };

  const [leftHandVisible, setLeftHandVisible] = useState(false);
  const [rightHandVisible, setRightHandVisible] = useState(false);
  const handVisiblity = {
    left: leftHandVisible,
    right: rightHandVisible,
  };
  const setHandFunctions = {
    Left: setLeftHand,
    Right: setRightHand,
  };

  const videoElement = useRef();
  const [thumbPosition, api] = useSpring(() => ({
    from: { position: [0, 0, 0] },
    config: {
      mass: 1,
      tension: 100,
      friction: 25,
    },
  }));
  // thumbPosition.set({ position: [1, 0, 0] });
  const hands = new Hands({ locateFile: (file) => `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}` });

  const startCamera = () => {
    setCameraStatus('starting');
    const cam = new Camera(videoElement.current, {
      facingMode: 'user',
      width: 1280,
      height: 720,
      onFrame: async (e) => {
        console.log(e);
        now = new Date();
        if (now - lastFrameTime > timeThreshold) {
          await hands.send({ image: videoElement.current });
          lastFrameTime = new Date();
        }
      },
    });
    cam.start().then(() => {
      setCamera(cam);
      setCameraStatus('started');
    });
  };

  const stopCamera = () => {
    camera.stop().then(() => {
      setCamera(null);
      setCameraStatus('stopped');
    });
  };

  const toggleCamera = () => {
    if (cameraStatus === 'stopped') startCamera();
    else stopCamera();
  };

  hands.setOptions({
    maxNumHands: 2,
    modelComplexity: 0.5,
    minDetectionConfidence: 0.75,
    minTrackingConfidence: 0.75,
  });

  hands.onResults(({ multiHandLandmarks, multiHandedness }) => {
    multiHandLandmarks.forEach((hand, index) => {
      setHandFunctions[multiHandedness[index].label](hand);
    });
    setLeftHandVisible(multiHandedness.some((item) => item.label === 'Left'));
    setRightHandVisible(multiHandedness.some((item) => item.label === 'Right'));
  });

  return (
    <div className="app">
      <Canvas>
        <ambientLight intensity={1} />
        <directionalLight intensity={10} position={[-10, -10, -10]} color="gray" />
        <Physics gravity={[0, 2, 0]}>
          <MarchingCubes resolution={64} maxPolyCount={20000} enableUvs={false} enableColors>
            <meshPhysicalMaterial vertexColors roughness={0.1} transmission={0.1} opacity={0.75} transparent />
            <MetaBall color="red" position={[1, 1, 0.5]} groups={interactionGroups(0, [0, 1])} />
            <MetaBall color="red" position={[-1, -1, -0.5]} groups={interactionGroups(0, [0, 1])} />
            <MetaBall color="red" position={[2, 2, 0.5]} groups={interactionGroups(0, [0, 1])} />
            <MetaBall color="blue" position={[-2, -2, -0.5]} groups={interactionGroups(1, [0, 1])} />
            <MetaBall color="blue" position={[3, 3, 0.5]} groups={interactionGroups(1, [0, 1])} />
            <MetaBall color="blue" position={[-3, -3, -0.5]} groups={interactionGroups(1, [0, 1])} />
            {/* <Pointer color="green" /> */}
          </MarchingCubes>
          <group key="hands">
            {
           Object.keys(handMeshes).map((handKey) => (
             <group
               key={handKey}
               visible={handVisiblity[handKey]}
              // rotation={[Math.PI, Math.PI, 0]}
              //  position={[0.5, 0.5, 0]}
             >
               {
                handMeshes[handKey].joints.map((joint) => {
                  const position = _hands[handKey][joint.index]
                    ? new Vector3(
                      _hands[handKey][joint.index].x * -2 + 1,
                      _hands[handKey][joint.index].y * -2 + 1,
                      _hands[handKey][joint.index].z,
                    )
                    : new Vector3();

                  return (
                    <MetaBall2
                      key={joint.key}
                      color="black"
                      position={position}
                    />
                  );
                })
              }{
                handMeshes[handKey].bones.map((bone) => {
                  const startPosition = _hands[handKey][bone.startIndex]
                    ? new Vector3(
                      _hands[handKey][bone.startIndex].x * -2 + 1,
                      _hands[handKey][bone.startIndex].y * -2 + 1,
                      _hands[handKey][bone.startIndex].z,
                    )
                    : new Vector3();

                  const endPosition = _hands[handKey][bone.endIndex]
                    ? new Vector3(
                      _hands[handKey][bone.endIndex].x * -2 + 1,
                      _hands[handKey][bone.endIndex].y * -2 + 1,
                      _hands[handKey][bone.endIndex].z,
                    )
                    : new Vector3();

                  const obj = new Object3D();
                  obj.position.set(startPosition.x, startPosition.y, startPosition.z);
                  obj.lookAt(endPosition);
                  const distance = startPosition.distanceTo(endPosition);
                  return (
                    <group
                      key={bone.key}
                      position={startPosition}
                      rotation={[obj.rotation.x, obj.rotation.y, obj.rotation.z]}
                    >
                      <mesh scale={[0.0125, distance, 0.0125]} position={[0, 0, distance / 2]} rotation={[Math.PI / 2, 0, 0]}>
                        <cylinderGeometry />
                        <meshStandardMaterial color="pink" />
                      </mesh>
                    </group>
                  );
                })
              }
             </group>
           ))
          }
          </group>
        </Physics>

        <Sky distance={450000} sunPosition={[0, -1, 0]} inclination={0} azimuth={0.25} />
        <Environment files={envFile} />
        {/* Zoom to fit a 1/1/1 box to match the marching cubes */}
        <Bounds fit clip observe margin={1}>
          <mesh visible={false}>
            <boxGeometry />
          </mesh>
        </Bounds>
      </Canvas>
      <video ref={videoElement} playsInline muted autoPlay className={`webcam ${cameraStatus}`} />
      <button className={`start-button ${cameraStatus}`} type="button" onClick={toggleCamera}>{cameraStatus === 'stopped' ? 'Start' : 'Stop'} Camera</button>
    </div>
  );
}
