import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

export default defineConfig({
  plugins: [react()],
  assetsInclude: ['**/*.glb', '**/*.hdr', '**/*.ttf'],
  base: '/portfolio-web/',
  server: {
    port: 8080,
    host: true,
  },
  build: { minify: false },
});
